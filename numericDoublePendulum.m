%clear; clc; %close all

%% Read the DH Table file and get the information
[DH, mass, Lc, I, u, grav] = parseDHFile("DoublePendulum_DH");

links = size(DH, 1);

Y0 = zeros([links*2 1]);
Y0(1:2:links*2) = extractVariablesFromDH(DH);
Y0(2:2:links*2) = 0;




%% Simluate
disp("Simulating")
n=10000;
t_span=linspace(0,5,n);


opt = odeset('OutputFcn',@odeprog,'Events',@odeabort);
[t_num,y_num] = ode45(@(t,y) numericODEfromDH(DH, y, Lc, mass, I, grav, [0; 0], 0), ...
    t_span, Y0, opt);

%% Animate
syms q [links 1] real

symDH = applyJointVartoDH(DH, q);

animateRobot(symDH, q', y_num(:,[1 3 5]), 't', t_num);
    