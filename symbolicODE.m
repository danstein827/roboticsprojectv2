function dy = symbolicODE(t, y, D, C, G, Q, u, lockMotor, maxV)
	links = size(D,1);
	
	if nargin < 7
		u = zeros([links 1]);
	end
	
	if nargin < 8
		lockMotor = 0;
    end
    
    if nargin < 9
        maxV = NaN;
    end
	
	dy = zeros([2*links 1]);
    dq = y(2:2:2*links);
	
	C = double(subs(C, Q, y));
	D = double(subs(D, Q, y));
	G = double(subs(G, Q, y));
	
	ddq = D\(u - C*dq - G);
	
	dy(1:2:2*links) = dq;
	dy(2:2:2*links) = ddq;
	
	if lockMotor
        indx = find(u==0);
        dy(2*indx-1) = 0;
		dy(indx*2) = 0;
    end
    
    if not(isnan(maxV))
        for i = 1:links
            if abs(dy(2*i-1)) > maxV
                dy(2*i-1) = sign(dy(2*i-1))*maxV;
                dy(2*i) = 0;
            end
        end
    end
	
end