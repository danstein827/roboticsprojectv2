function C = getSymbolicC(D, q, dq, smp)
    % Finds the C matrix (from christoffel symbols) for provided Inertia
    % matrix (D) with joint variables (q) and joint velocities (dq)
    % Used only for symbolic matricies
    
    if nargin == 3
        smp = 0;
    end
    
    links = size(D, 1);
    
    dD = sym(zeros([links links links]));    
    christoffel = dD;
    C = sym(zeros([links links]));
    
    for i = 1:links
        if not((isnan(q(i))) || q(i) == 0)
            % Differentiate D matrix by qi
            dD(:,:,i) = diff(D, q(i));
        end
    end
    
    for i = 1:links
        for j = 1:links
            for k = 1:links
                christoffel(i,j,k) = 0.5*(dD(k,j,i) + dD(k,i,j) - dD(i,j,k));
				C(k,j) = C(k,j) + christoffel(i,j,k)*dq(i);
            end
        end
    end    
    
    if smp
        C = simplify(C);
    end
    
end