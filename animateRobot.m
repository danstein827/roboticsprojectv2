function [path, time] = animateRobot(DH, q, y, varargin)
    figure();
    ax = axes();
    
    links = size(DH, 1);
    T = size(y, 1);
    
    p = inputParser;
    addParameter(p, 't', 1:T);
    addParameter(p, 'Scale', 1);
    addParameter(p, 'Range', [-5 5]);
    addParameter(p, 'FrameSkip', 50);
    parse(p, varargin{:});
        
    t = p.Results.t;
    scale = p.Results.Scale;
    pRange = p.Results.Range;
    pTicks = linspace(pRange(1), pRange(2), 11);
    frameSkip = p.Results.FrameSkip;
    
    H = HfromDHTable(DH, 'OnlyFinal', 0, 'IsDeg', 0);

    h = subs(H, q, y(1,:));

    r = [];
    r(1,:) = [0 0 0];

    for i = 1:links
        r(i+1,:) = h(1:3,4,i)';
    end

    plot3(ax, r(:,1), r(:,2), r(:,3), 'b-o')
    hold on
    % plotHomogeneousTransform(ax, h(:,:,end), [0; 0; 0], scale);
    txt = annotation('textbox', [.8 .85 .1 .1], 'String', 'Time: 0.00s');

    xlim(pRange); ylim(pRange); zlim(pRange);
    xticks(pTicks); yticks(pTicks); zticks(pTicks)
    xlabel('x'); ylabel('y'); zlabel('z')
    pbaspect([1 1 1])
    grid on

    pause
    
    path = [];
    time = [];

    for c = 1:T
        if mod(c, frameSkip) == 0
            cla
            h = subs(H, q, y(c,:));

            r(1,:) = [0 0 0];

            for i = 1:links
                r(i+1,:) = h(1:3,4,i)';
            end

            path(end+1, :) = r(end, :);
            time(end+1) = t(c);
            
            txt.String = sprintf("t: %.2f s", t(c));
            
            plot3(ax, r(:,1), r(:,2), r(:,3), 'b-o')
            plot3(ax, path(:,1), path(:,2), path(:,3), 'r-')
            % plotHomogeneousTransform(ax, h(:,:,end), [0; 0; 0], scale);

            pause(0.01)
        end
    end

    hold off
