function DHout = applyJointVartoDH(DH, q)
    % Returns DH table after applying joint variables (q) to the respective
    % joint rows

    links = size(DH, 1);

    if length(q) ~= links
        error("q length must equal DH rows." + ...
                "\n\tq length\t%d \n\tDH rows\t\t%d", ...
                length(q), size(DH,1))
    end
    
    if isa(DH, 'sym') || isa(q, 'sym')
		type = 'sym';
	else
		type = 'double';
    end
    
    DHout = zeros(size(DH), type);
    
    for i = 1:links
        in = DH(i, 1)+2;
        DHout(i, :) = DH(i, :);
        
        if (in > 1) && (in <= 5)
            DHout(i, in) = q(i);
        end
    end
end