function D = getInertiaMatrix(J, H, m, I, smp)
	% Returns the inertia matrix D(q)
	% J = Jacobian for each link
	% H = Homogeneous Transform of each link
	% m = link mass vector
	% I = Inertia Tensors for each link
	
    if nargin == 4
        smp = 0;
    end
    
	links = size(J,3);
	
	if isa(J, 'sym') || isa(H, 'sym')
		type = 'sym';
	else 
		type = 'double';
	end
	
	D = zeros([links links], type);
	
    for i = 1:links
		Dlin = m(i)*J(1:3,:,i)'*J(1:3,:,i);		%Linear Velocity Term
		Drot = J(4:6,:,i)'*H(1:3,1:3,i)*I(:,:,i) ...
                *H(1:3,1:3,i)'*J(4:6,:,i);		% Rotational Velocity Term
			
		D = D + Dlin + Drot;
    end
    
    if isa(D, 'sym') && smp
        D = simplify(D);
    end
    
end