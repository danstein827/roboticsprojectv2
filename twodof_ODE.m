function dy = twodof_ODE(t,y)

u=[0 0]';
%function dy = twodof_ODE(t,y,u)
%y1 = y(1);
%y2 = y(2);
%y3 = y(3);
%y4 = y(4);
%u1 = u(1);
%u2 = u(2);

l1=1;
l2=1;
m1=1;
m2=1;
g=9.8;
lc1 = l1/2;
lc2 = l2/2;

Iz1 = m1*(l2^2)/12;
Iz2 = m2 *(l2^2)/12;
D = zeros(2,2);


D(1,1) = m1*(lc1^2) + m2*(l1^2+lc2^2+2*l1*lc2*cos(y(3))) + Iz1 +Iz2;
D(1,2) = m2 * (lc2^2 + l1*lc2*cos(y(3))) + Iz2;
D(2,1) = D(1,2);
D(2,2) = m2*lc2^2 + Iz2;

h = -m2*l1*lc2*sin(y(3));
C = zeros(2,2);
C = [h*y(4), h*y(4)+h*y(2); -h*y(2), 0];


g1 = (m1*lc1 +m2*l1)*g*cos(y(1)) + m2*lc2*g*cos(y(1)+y(3));
g2 = m2*lc2*g*cos(y(1)+y(3));
G = [g1;g2];

% D*ydotdot + C * ydot + G = u;
ydot = [y(2); y(4)];
ydotdot = D\(u - C * ydot - G);

dy1 = ydot(1);
%dy2 = (2*l2*u1 - 2*l2*u2 + sin(y3)*(2*l1*m2*l2^2*y2^2 + 4*l1*m2*l2^2*y2*y4 + 2*l1*m2*l2^2*y4^2) - 2*l1*u2*cos(y3) - 2*l1*l2*m1*g*cos(y1) - l1*l2*m2*g*cos(y1) + l1*l2*m2*g*cos(y1 + 2*y3) + l1^2*l2*m2*y2^2*sin(2*y3))/(l1^2*l2*(2*m1 + m2 - m2*cos(2*y3)));
dy2 = ydotdot(1);
dy3 = ydot(2);
dy4 = ydotdot(2);

%dy4 = -(l2^2*m2*u1 - l1^2*m2*u2 - l1^2*m1*u2 - l2^2*m2*u2 - l1*l2^2*m2^2*g*cos(y1) + l1^2*l2^2*m2^2*y2^2*sin(2*y3) + (l1^2*l2^2*m2^2*y4^2*sin(2*y3))/2 + l1*l2*m2*u1*cos(y3) - 2*l1*l2*m2*u2*cos(y3) + l1*l2^3*m2^2*y2^2*sin(y3) + l1^3*l2*m2^2*y2^2*sin(y3) + l1*l2^3*m2^2*y4^2*sin(y3) - l1*l2^2*m1*m2*g*cos(y1) - l1^2*l2*m2^2*g*sin(y1)*sin(y3) + l1*l2^2*m2^2*g*cos(y1)*cos(y3)^2 + l1^2*l2^2*m2^2*y2*y4*sin(2*y3) + l1^3*l2*m1*m2*y2^2*sin(y3) + 2*l1*l2^3*m2^2*y2*y4*sin(y3) - l1*l2^2*m2^2*g*cos(y3)*sin(y1)*sin(y3) - l1^2*l2*m1*m2*g*sin(y1)*sin(y3))/(l1^2*l2^2*m2*(m1 + m2 - m2*cos(y3)^2));

dy = [dy1 dy2 dy3 dy4]'; 


