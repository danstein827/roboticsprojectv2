function q = extractVariablesFromDH(DH)
    links = size(DH, 1);
    q = zeros([links 1]);
    
    for i = 1:links
        if DH(i,1) >= 0
            q(i) = DH(i, DH(i, 1)+2);
        else
            q(i) = NaN;
        end
    end
end