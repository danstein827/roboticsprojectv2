function R = RotationMatrix(cmd, order, isDeg)
    % Returns a rotation matrix
    % Rotates cmd(i) about order(i) axis

    if nargin == 2
        isDeg = 1;
    end
    
    if isDeg
        cmd = deg2rad(cmd);
    end
    
    R = eye(3);
    
    if all((size(cmd)==size(order))&(size(cmd, 1)==1))
        for i = 1:length(cmd)
            switch order(i)
                case 'x'
                    r = [1 0 0; ...
                        0 cos(cmd(i)) -sin(cmd(i)); ...
                        0 sin(cmd(i)) cos(cmd(i))];
                case 'y'
                    r = [cos(cmd(i)) 0 sin(cmd(i)); ...
                        0 1 0; ...
                        -sin(cmd(i)) 0 cos(cmd(i))];
                case 'z'
                    r = [cos(cmd(i)) -sin(cmd(i)) 0;
                        sin(cmd(i)) cos(cmd(i)) 0;
                        0 0 1];
            end
            
            R = R*r;
        end
    end
end