function [DH, mass, Lc, I, u, grav] = parseDHFile(fileName)
    m = readmatrix(fileName);
    links = size(m, 1);
    
    %DH Table
    DH = m(:, 2:6);
    %Mass vector
    mass = m(:, 7);
    %Vectors to center of masses
    Lc = m(:, 11:13)';
    %Inertia Tensors
    I = zeros([3 3 links]);
    for i = 1:links
        I(:,:,i) = diag(m(i, 14:16));
    end
    u = m(:, 17);
    grav = m(1, 18:20)';