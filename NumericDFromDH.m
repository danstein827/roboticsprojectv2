function [D, H] = NumericDFromDH(DH, q, Lc, m, I, isDeg)
    % Returns inertia matrix, D(q), and homogeneous transform H of each
    % link.  For use in numerical algorithm

    if nargin == 4
        isDeg = 1;
    end
    
    jDH = double(applyJointVartoDH(DH, q));
    
    links = size(DH, 1);
    
    J = zeros([6 links links]);
    D = zeros([links links]);
    
    H = HfromDHTable(jDH, 'OnlyFinal', 0, 'IsDeg', 0);
    
    for i = 1:links
        J(:,:,i) = jacobianFromDH(jDH(1:i, :), 'Nlinks', links, ...
                'Centerofmass', Lc(:,i), 'HMatrix', H(:,:,1:i), ...
                'IsDeg', isDeg);
    end
    
    D = getInertiaMatrix(J, H, m, I);
end