clear; clc; %close all

%% Read the DH Table file and get the information
[DH, mass, Lc, I, u, grav] = parseDHFile("DoublePendulum_DH");

links = size(DH, 1);

syms q [links 1] real
syms dq [links 1] real

Q = zeros([links*2 1], 'sym');
Q(1:2:links*2) = q;
Q(2:2:links*2) = dq;

% Get initial joint variables
y0 = extractVariablesFromDH(DH);

symDH = applyJointVartoDH(DH, q);

%% Get the Jacobian
disp("Finding jacobian")
J = zeros([6 links links], 'sym');
for i = 1:links
    [J(:,:,i), H] = jacobianFromDH(symDH(1:i, :), 'IsDeg', 0, 'Nlinks', links, ...
        'CenterofMass', Lc(:,i), 'Simplify', 1);
end

%% Get inertia matrix (D(q))
disp("Finding inertia matrix")
D = getInertiaMatrix(J, H, mass, I, 1);

%% Get C matrix (C(q, dq)).  Derived from Christoffel symbols
disp("Finding C matrix")
C = getSymbolicC(D, q, dq, 1);

%% Get G Matrix (G(q)).  The poential energy terms
disp("Finding potential energy")
G = getSymbolicG(H,q,mass,Lc,grav,1);

%% First we must remove the rows and columns that correspond with 
%  non-dynamic frames

for i = fliplr(find(isnan(y0)))
    D(i,:) = [];
    D(:,i) = [];
    C(i,:) = [];
    C(:,i) = [];
    G(i) = [];
    y0(i) = [];
    Q(2*i) = [];
    Q(2*i-1) = [];
    u(i) = [];
end

%% Simluate
disp("Simulating")
n=10000;
t_span=linspace(0,5,n);

x0 = zeros([length(y0)*2 1]);
x0(1:2:2*length(y0)) = y0;
x0(2:2:2*length(y0)) = 0;


opt = odeset('OutputFcn',@odeprog,'Events',@odeabort);
[t_sym,y_sym] = ode45(@(t,y) symbolicODE(t, y, D, C, G, Q, u, 0), t_span, x0, opt);

%% Animate
animateRobot(symDH, Q', y_sym, 't', t_sym);
    