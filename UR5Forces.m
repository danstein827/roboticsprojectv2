function u = UR5Forces(t)
    command = [0   8  20;
               5  12  15;
               8  15  10;
               10 15   5;
               10 15   5];

    u = zeros([6 1]);
    
    tri = @(p, t) 1 - abs(mod(t, 2*p) - p)/p;
    
    for i = 1:size(command,1)
        if (t >= command(i,1)) && (t <= command(i,2))
            m  = (command(i,2) - command(i,1))/2;
            u(i) = command(i,3)*tri(m, t - command(i,1));
        end
    end
end