function P = getPotentialEnergy(H, Lc, m, g)
    % Calcuates the total potential energy of a robot
    
    links = size(H,3);
    
    P = 0;
    
    for i = 1:links
        P = P + m(i)*g'*H(1:3,:,i)*[Lc(:,i); 1];
    end