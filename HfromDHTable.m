function H = HfromDHTable(DH, varargin)
	% Returns the homogeneous transforms from a passed DH Table
    % DH containts the DH table, each row corresponds to a link.
    %   Order of columns: link variable (0 = rev about z, 1 = pris along z,
    %       2 = pris along x, 3 = rev about x), theta, d, a, alpha.
    %   Link variable can equal -1 for an intermediate link.  q matrix
    %       should have a placeholder variable for that ignored link.
	
	links = size(DH, 1);
	q = NaN;
	
	% Set up functions to check user inputs
    validFlag = @(x) (x == 0) || (x == 1);
	
	p = inputParser;
    addOptional(p, 'JointVar', q);
    addParameter(p, 'OnlyFinal', 1);
    addParameter(p, 'IsDeg', 1, validFlag);
    addParameter(p, 'Simplify', 0, validFlag);
    parse(p, varargin{:});
	
	q = p.Results.JointVar;	% Passed joint variables
	onlyFinal = p.Results.OnlyFinal; % Return only end effector transforms?
	flagDeg = p.Results.IsDeg;		% DH table in degrees?
	
	if isa(DH, 'sym') || isa(q, 'sym')
		type = 'sym';
	else
		type = 'double';
	end
	
	% Preallocate H array
	if onlyFinal
		H = eye(4, type);
	else
		H = zeros([4 4 links], type);
	end
	
	if not(isnan(q))
		DHvar = applyJointVartoDH(DH, q);
    else
        DHvar = DH;
	end
	
	for i = 1:links
		h = HfromDHLine(DHvar(i, 2:5), flagDeg);
		
		if onlyFinal
			H = H*h;
		else
			if i == 1
				H(:,:,1) = h;
			else
				H(:,:,i) = H(:,:,i-1)*h;
			end
		end
    end
    
    if (type(1) == 's') && p.Results.Simplify
        H = simplify(H);
    end
end