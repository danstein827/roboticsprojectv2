function dy = tst_ODE(t,y0)
%u=[0 0]';
syms l1 l2 m1 m2 g lc1 lc2 Iz1 Iz2 D C y t R11 R12 R21 R22 h y1 y2 y3 y4 u1 u2 u
Iz1 = m1*(l2^2)/12;
Iz2 = m2 *(l2^2)/12;
%D = zeros(2,2);
y = [y1;y2;y3;y4];
u = [u1;u2];
R11 = m1*(lc1^2)+m2*(l1^2+lc2^2+2*l1*lc2*cos(y(3)))+Iz1+Iz2;
R12 = m2*(lc2^2+l1*lc2*cos(y(3)))+Iz2; 
R21 = m2*(lc2^2+l1*lc2*cos(y(3)))+Iz2;
R22 = m2*lc2^2 + Iz2;
D = [R11,R12;R21,R22];
h = -m2*l1*lc2*sin(y(3));
C = zeros(2,2);
C = [h*y(4), h*y(4)+h*y(2); -h*y(2), 0];


g1 = (m1*lc1 +m2*l1)*g*cos(y(1)) + m2*lc2*g*cos(y(1)+y(3));
g2 = m2*lc2*g*cos(y(1)+y(3));
G = [g1;g2];

% D*ydotdot + C * ydot + G = u
ydot = [y(2); y(4)];
ydotdot = D\(u - C * ydot - G);
dy1 = ydot(1);

dy2 = ydotdot(1);
dy3 = ydot(2);
dy4 = ydotdot(2);


%dy = [dy1, dy2, dy3, dy4]'; 
u1 = 1;
u2 = 0;
l1=1;
l2=1;
m1=1;
m2=1;
g=9.8;
lc1 = l1/2;
lc2 = l2/2;
y1 = y0(1); 
y2 = y0(2); 
y3 = y0(3); 
y4 = y0(4);
Iz1 = eval(Iz1);
Iz2 = eval(Iz2);
dy = [eval(dy1);eval(dy2);eval(dy3);eval(dy4)];

