-- This is a threaded script, and is just an example!

local csvfile = require "simplecsv"
local c = require "const"
local simTime = 20

function sysCall_threadmain()
    jointHandles={-1,-1,-1,-1,-1,-1}
    for i=1,6,1 do
        jointHandles[i]=sim.getObjectHandle('UR5_joint'..i)
    end
	
	data = {}
	i = 0
	
	
	maxTime = 20
	t = simGetSimulationTime()
	while t <= maxTime do
		t = simGetSimulationTime()
		
		i = i+1
		data[i] = {}
		
		for j=1,6,1 do
			data[i][j] = simGetJointPosition(jointHandles[j])
		end
		
		sim.wait(0.005)
	end
	
	csvfile.write(c.path..'Joint.txt', data)
	
	
end