--constants
constants = {}

do
	constants['deg2rad'] = math.pi/180
	constants['rad2deg'] = 1/constants.deg2rad
	constants['path'] = debug.getinfo(1).source:match("@?(.*/)")
	constants['simTime'] = 15

	return constants
end