-- This is a threaded script, and is just an example!

function triangle(m, t)
	tri = 1 - math.abs(t % (2*m) - m)/m

	return tri
end

commands = {{0, 8, 20}, {5, 12, 15}, {8, 15, 10}, {10, 15, 5}, {10, 15, 5}}


function sysCall_threadmain()
    jointHandles={-1,-1,-1,-1,-1,-1}
    for i=1,6,1 do
        jointHandles[i]=sim.getObjectHandle('UR5_joint'..i)
    end
	
	
	
	
	maxTime = 20
	t = simGetSimulationTime()
	while t <= maxTime do
		msg = ""
		t = simGetSimulationTime()
		
		for i = 1,5 do
			if (t >= commands[i][1]) and (t <= commands[i][2]) then
				m = (commands[i][2]-commands[i][1])/2
				f = commands[i][3]*triangle(m, t-commands[i][1])
				simSetJointTargetVelocity(jointHandles[i], 10)
				simSetJointForce(jointHandles[i], f)
				msg = msg .. "Joint " .. i .. ". "
			else
				simSetJointTargetVelocity(jointHandles[i], 0)
			end
		end		
		print(msg)
		sim.wait(0.005)
	end
	
	
end