-- if you wish to execute code contained in an external file instead,
-- use the require-directive, e.g.:
--
-- require 'myExternalFile'
--
-- Above will look for <V-REP executable path>/myExternalFile.lua or
-- <V-REP executable path>/lua/myExternalFile.lua
-- (the file can be opened in this editor with the popup menu over
-- the file name)

local csvfile = require "simplecsv"
local c = require "const"
local simTime = 5

function sysCall_threadmain()
    -- Put some initialization code here
	data = {}
	i = 0
	
	endEffector = sim.getObjectHandle('UR5_link7')
    base = sim.getObjectHandle('UR5_link1_visible')
	
	if endEffector then
		while sim.getSimulationTime() < simTime do
			i = i + 1
            data[i] = {}
			data[i][1] = sim.getSimulationTime()
			pos = sim.getObjectPosition(endEffector, base)
			data[i][2] = pos[1]
			data[i][3] = pos[2]
			data[i][4] = pos[3]
			sim.wait(0.01)
		end
		
		csvfile.write(c.path..'Position.txt', data)
	else
		print("End effector not found")
	end

end

function sysCall_cleanup()
    -- Put some clean-up code here
end