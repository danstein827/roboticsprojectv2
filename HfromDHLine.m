function H = HfromDHLine(line, isDeg)
    % Returns homogeneous transform from a line of a DH table
    % line is 1x4 array.
    % line(1) is theta  (about current z)
    % line(2) is d  (along current z)
    % line(3) is a  (along current x)
    % line(4) is alpha  (about current x)

	if nargin == 1
		isDeg = 1;
	end
	
	if isDeg
		line([1 4]) = deg2rad(line([1 4]));
	end
	
	theta =  [RotationMatrix(line(1), 'z', 0) [0; 0; 0]; 0 0 0 1];
	d_a = [eye(3) [line(3); 0; line(2)]; 0 0 0 1];
	alpha = [RotationMatrix(line(4), 'x', 0) [0; 0; 0]; 0 0 0 1];
    
    H = theta*d_a*alpha;
	
end