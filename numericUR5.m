%clear; clc; close all

%% Read the DH Table file and get the information
[DH, mass, Lc, I, u, grav] = parseDHFile("UR5_DH.xlsx");

links = size(DH, 1);

Y0 = zeros([links*2 1]);
Y0(1:2:links*2) = extractVariablesFromDH(DH);




%% Simluate
disp("Simulating")
dt = 0.0005;
t_span=0:dt:20;


opt = odeset('OutputFcn',@odeprog,'Events',@odeabort);
[t_num,y_num] = ode23(@(t,y) numericODEfromDH(DH, y, Lc, mass, I, grav, ...
    UR5Forces(t), 0, 1, 10), t_span, Y0, opt);

%% Animate
syms q [links 1] real

symDH = applyJointVartoDH(DH, q);

[path_num, t_num_Ani] = animateRobot(symDH, q', y_num(:,1:2:2*links), 't', t_num, ...
    'Range', [-1.2 1.2], 'FrameSkip', 100);

    