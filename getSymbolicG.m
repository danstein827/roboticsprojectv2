function G = getSymbolicG(H, q, m, lc, g, smp)
    % Returns G(q) for the potential energy matrix
    
    if nargin == 5
        smp = 0;
    end
    
    links = size(H,3);
    
    P = sym(0);
    for i = 1:links
        if not(isnan(q(i)) || q(i) == 0)
            P = P + m(i)*g'*H(1:3,:,i)*[lc(:,i); 1];
        end
    end
    
    G = sym(zeros([links 1]));

    for i = 1:links
        if not(isnan(q(i)) || q(i) == 0)
            G(i) = diff(P, q(i));
        end
    end 
    
    if isa(G, 'sym') && smp
        G = simplify(G);
    end
    
end