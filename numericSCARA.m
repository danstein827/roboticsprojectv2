clear; clc; %close all

%% Read the DH Table file and get the information
[DH, mass, Lc, I, u, grav] = parseDHFile("SCARA_DH.xlsx");

links = size(DH, 1);

Y0 = zeros([links*2 1]);
Y0(1:2:links*2) = extractVariablesFromDH(DH);




%% Simluate
disp("Simulating")
dt = 0.0005;
t_span=0:dt:10;


opt = odeset('OutputFcn',@odeprog,'Events',@odeabort);
[t_num,y_num] = ode45(@(t,y) numericODEfromDH(DH, y, Lc, mass, I, grav, u, 0), ...
    t_span, Y0, opt);

%% Animate
syms q [links 1] real

symDH = applyJointVartoDH(DH, q);

animateRobot(symDH, q', y_num(:,[1 3 5 7]), 't', t_num);
    