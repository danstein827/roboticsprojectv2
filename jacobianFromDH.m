function [J, H] = jacobianFromDH(DH, varargin)
	% Takes a set of homogeneous transforms and returns
	%	the jacobain matrix
	% J is a 6xi array where i is the number of links
	
	links = size(DH, 1);		% Get number of links
	
	% functions to validate user inputs
    validN = @(x) isscalar(x) && x > 0;
    validFlag = @(x) (x == 0) || (x == 1);
    validVector = @(x) (isnumeric(x) || isa(x, 'sym')) && ...
                    (all(size(x) == [3 1]) || (all(size(x) == [1 3])));
	
	p = inputParser;
    addParameter(p, 'Nlinks', links, validN);
    addParameter(p, 'IsDeg', 1, validFlag);
    addParameter(p, 'Centerofmass', [0;0;0], validVector);
    addParameter(p, 'HMatrix', NaN);
    addParameter(p, 'Simplify', 0, validFlag);
    parse(p, varargin{:});
    
    com = p.Results.Centerofmass;
	nlinks = p.Results.Nlinks;
	
	if isa(DH, 'sym')
		type = 'sym';
	else
		type = 'double';
	end
	
	
	% Preallocate the jacobian
	J = zeros([6 nlinks], type);
	
    if isnan(p.Results.HMatrix)
        H = HfromDHTable(DH, 'OnlyFinal', 0, 'IsDeg', p.Results.IsDeg);
    else
        H = p.Results.HMatrix;
    end
	
	for i = 1:links
		jnt = double(DH(i,1));
		
		switch jnt
			case 0	% Revolute joint about z axis
				if i == 1
					z = [0; 0; 1];
					O = H(1:3,:,end)*[com; 1];
				else
					z = H(1:3,3,i-1);
					O = H(1:3,:,end)*[com; 1] - H(1:3,4,i-1);
				end
				J(1:3, i) = cross(z, O);	%Linear Term
				J(4:6, i) = z;				%Rotational Term
				
			case 1 % Prismatic Joint along z axis
				if i == 1
					z = [0; 0; 1];
				else
					z = H(1:3,3,i-1);
				end
				J(1:3, i) = z;		% Linear Term
				
			case 2 % Prismatic Joint along x axis
				if i == 1
					x = [1; 0; 0];
				else
					x = H(1:3,1,i-1);
				end
				J(1:3, i) = x;		%Linear Term
				
			case 3 % Revolute joint about x axis
				if i == 1
					x = [1; 0; 0];
					O = H(1:3,:,end)*[com; 1];
				else
					x = H(1:3,1,i-1);
					O = H(1:3,:,end)*[com; 1] - H(1:3,4,i-1);
				end
				J(1:3, i) = cross(x,O);		%Linear Term
				J(4:6, i) = x;				%Rotational term
				
			otherwise		%Non-dynamic frame
			
		end
    end
    
    if isa(J, 'sym') && p.Results.Simplify
        J = simplify(J);
    end
    
end