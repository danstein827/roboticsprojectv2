function dy = numericODEfromDH(DH, y, Lc, m, I, grav, u, isDeg, lockMotor, maxV)
    if nargin < 9
        lockMotor = 0;
    end

    if nargin < 10
        maxV = NaN;
    end
    

    links = size(DH, 1);
    
    dy = zeros([links*2 1]);
    
    q = y(1:2:links*2);
    dq = y(2:2:links*2);
    
    dy(1:2:2*links) = dq;
    
    [D, H] = NumericDFromDH(DH, q, Lc, m, I, isDeg);
    [C, G] = NumericCG(DH, D, H, q, dq, Lc, m, I, grav, isDeg);
    
    indx = find(not(ismember(DH(:,1), 0:3)));
    
    for i = fliplr(indx)
        D(:,i) = [];
        D(i,:) = [];
        C(:,i) = [];
        C(i,:) = [];
        G(i) = [];
        u(i) = [];
        dq(i) = [];
    end
    
    ddq = D\(u - C*dq - G);
    
    c = 1;
    
    for i = 1:links
        if not(ismember(i,indx))
            dy(2*i) = ddq(c);
            c = c+1;
        end
    end
    
    if lockMotor
        for i = find(u == 0)
            dy(2*i-1) = 0;
            dy(2*i) = 0;
        end
    end
    
    if not(isnan(maxV))
        for i = 1:links
            if abs(dy(2*i-1)) > maxV
                dy(2*i-1) = sign(dy(2*i-1))*maxV;
                dy(2*i) = 0;
            end
        end
    end
    
end
    
    
    