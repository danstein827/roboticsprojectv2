function [C, G] = NumericCG(DH, D, H, q, dq, Lc, m, I, grav, isDeg)
    % Estimates the C(q, dq) matrix via Euler method.
    %  Christoffel symbols are estimated by calculating new D(q) with a
    %  slightly changed q variable to get D(q)/dqi
    % Similiarly estimates the potetial energy G(q) matrix
    
    links = size(DH, 1);
    
    if nargin == 8
        isDeg = 1;
    end
    
    G = zeros([links 1]);
    dd = zeros([links links links]);
    C = zeros([links links]);
    P = getPotentialEnergy(H, Lc, m, grav);
    
    eps = 0.00001;
    
    for i = 1:links
        q_eps = q;  q_eps(i) = q_eps(i) + eps;
        [D_eps, H_eps] = NumericDFromDH(DH, q_eps, Lc, m, I, isDeg);
        dd(:,:,i) = (D_eps - D)/eps;
        
        P_eps = getPotentialEnergy(H_eps, Lc, m, grav);
        G(i) = (P_eps - P)/eps;
    end
    
    for i = 1:links
        for j= 1:links
            for k = 1:links
                C(k,j) = C(k,j) + 0.5*(dd(k,j,i) + dd(k,i,j) - dd(i,j,k))*dq(i);
            end
        end
    end
end