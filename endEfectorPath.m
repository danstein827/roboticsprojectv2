function path = endEfectorPath(DH, y)
    path = zeros([length(y) 3]);
    
    links = size(DH, 1);
    
    syms q [links 1] real
    
    H = HfromDHTable(DH, 'JointVar', q, 'IsDeg', 0);

    for i = 1:length(y)
        h = subs(H, q', y(i, :));
        path(i,:) = h(1:3,4)';
    end
end